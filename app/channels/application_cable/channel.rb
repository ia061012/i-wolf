module ApplicationCable
  class Channel < ActionCable::Channel::Base
    private

    def reset_channel_name(room_id)
      "reset_room_#{room_id}"
    end

    def result_channel_name(room_id)
      "result_room_#{room_id}"
    end
  end
end
