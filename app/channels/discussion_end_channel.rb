class DiscussionEndChannel < ApplicationCable::Channel
  def subscribed
    stream_for current_user
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def voting
    round = current_user.room.reload.current_round
    return if round.voting?

    round.move_to_voting!
    current_user.room.users.each do |user|
      UsersChannel.broadcast_to(user, users_html: ApplicationController.new.helpers.render('rooms/users', current_user: user))
      DiscussionEndChannel.broadcast_to(user, nil)
    end
  end

  def wrong
    return if current_user.host?

    round = current_user.room.current_round
    round.close!
    result_html = ApplicationController.new.helpers.render('rooms/result', round: round)
    ActionCable.server.broadcast(result_channel_name(current_user.room.id), result_html: result_html)
  end
end
