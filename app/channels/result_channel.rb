class ResultChannel < ApplicationCable::Channel
  def subscribed
    stream_from result_channel_name(params['room_id'])
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def vote(data)
    voter = current_user
    votee = User.find(data['user_id'])
    room  = voter.room.reload #TODO: 何故かリロードしないと各リレーションが古いままになる。謎すぎて食パン殴りたい。
    round = room.current_round
    Vote.create(voter: voter, votee: votee, round: round)

    if room.all_users_has_voted?
      round.close!
      result_html = ApplicationController.new.helpers.render('rooms/result', round: round)
      ActionCable.server.broadcast(result_channel_name(current_user.room.id), result_html: result_html)
    else
      room.users.each do |user|
        UsersChannel.broadcast_to(user, users_html: ApplicationController.new.helpers.render('rooms/users', room: room, current_user: user))
      end
    end
  end
end
