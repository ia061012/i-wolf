class RoundResetChannel < ApplicationCable::Channel
  def subscribed
    stream_from reset_channel_name(params['room_id'])
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def reset
    return unless current_user.host?

    current_round = current_user.room.current_round
    current_round.abort if current_round&.playing? || current_round&.voting?
    ActionCable.server.broadcast(reset_channel_name(current_user.room.id), nil)
  end
end
