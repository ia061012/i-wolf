class RoundStartChannel < ApplicationCable::Channel
  def subscribed
    stream_for current_user
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def start(data)
    return unless current_user.host?

    # TODO: 最少催行人数を考慮

    # TODO: GameTypeをRoom紐付けではなくRound紐付けにする
    room = current_user.room.reload
    round = room.rounds.create!(state: Round::STATES[:playing])
    room.users.each do |user|
      RoundStartChannel.broadcast_to(user, theme_html: ApplicationController.new.helpers.render('rooms/theme', round: round, current_user: user), minutes: data['minutes'])
    end
  end
end
