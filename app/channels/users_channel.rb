class UsersChannel < ApplicationCable::Channel
  def subscribed
    return unless params['room_id']

    stream_for current_user
    room = Room.find(params['room_id'])
    current_user.enter(room)
    room.users.each do |user|
      data = { users_html: ApplicationController.new.helpers.render('rooms/users', current_user: user) }
      data.merge!(enter: current_user.name) if user != current_user
      UsersChannel.broadcast_to(user, data)
    end
  end

  def unsubscribed
    return unless current_user.room

    room = current_user.room
    current_user.leave
    room = room.reload

    # 進行中ならゲームをリセット
    if room.current_round&.playing? || room.current_round&.voting?
      room.current_round.abort
      ActionCable.server.broadcast(reset_channel_name(room.id), nil)
    end

    room.users.each do |user|
      UsersChannel.broadcast_to(user, users_html: ApplicationController.new.helpers.render('rooms/users', current_user: user), leave: current_user.name)
    end
  end

  def kick(data)
    kicked_user = User.find(data['user_id'])
    room = kicked_user.room
    # TODO: エラーメッセージのブロードキャスト
    return unless current_user.host?(room)

    kicked_user.leave
    UsersChannel.broadcast_to(kicked_user, users_html: '', kicked: true)
    room.users.each do |user| # キックされたユーザがルートに戻れば自動更新されるが、先に更新しておく
      UsersChannel.broadcast_to(user, users_html: ApplicationController.new.helpers.render('rooms/users', current_user: user))
    end
  end
end
