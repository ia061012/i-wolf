class RoomsController < ApplicationController
  before_action :set_room, only: [:show, :edit, :update]
  before_action :create_room, only: [:index]
  before_action :create_user, only: [:index]

  # GET /rooms
  def index; end

  # GET /rooms/1
  def show
    redirect_to :root, flash: { error: 'please log in' } and return unless logged_in?

    redirect_to :root, flash: { error: 'ゲーム進行中の部屋には入れません。' } and return if @room.playing? || @room.voting?
  end

  def create_room
    @room = Room.new
  end

  def create_user
    @user = User.new
  end

  # GET /rooms/1/edit
  def edit
  end

  # POST /rooms
  def create
    redirect_to :root, flash: { error: 'please log in' } and return unless logged_in?

    @room = Room.new(room_params.merge(host: current_user))
    if @room.save
      flash[:success] = 'Room was successfully created'
      redirect_to room_path(@room, password: @room.password)
    else
      flash.now[:error] = @room.errors_html
      render :index
    end
  end

  # PATCH/PUT /rooms/1
  # PATCH/PUT /rooms/1.json
  def update
    respond_to do |format|
      if @room.update(room_params)
        format.html { redirect_to @room, notice: 'Room was successfully updated.' }
        format.json { render :show, status: :ok, location: @room }
      else
        format.html { render :edit }
        format.json { render json: @room.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /rooms/1
  def destroy
    redirect_to :root, flash: { error: 'please log in' } and return unless logged_in?

    @room = Room.find_by(id: params[:id], host: current_user)
    redirect_to :root, flash: { error: '部屋が存在しません。' } and return unless @room

    redirect_to :root, flash: { error: 'ユーザが入室しています。' } and return if @room.users.count.positive?

    @room.destroy
    redirect_to :root, flash: { success: "部屋「#{@room.name}」を削除しました。" }
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_room
      params[:password] ||= ''
      @room = Room.find_by(id: params[:id], password: params[:password])
      unless @room
        flash[:error] = '部屋IDかパスワードが間違っています。'
        redirect_to :root
      end
    end

    # Only allow a list of trusted parameters through.
    def room_params
      params.require(:room).permit(:name, :password, :game_type_id)
    end
end
