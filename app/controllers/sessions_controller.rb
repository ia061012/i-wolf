class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by(name: params[:session][:name].downcase)
    if user && user.authenticate(params[:session][:password])
      log_in user
      remember user
      flash[:success] = 'You have logged in'
      redirect_to root_url
    else
      flash[:error] = 'Invalid name/password combination'
      redirect_to root_url
    end
  end

  def destroy
    log_out if logged_in?
    flash[:success] = 'You have logged out successfully.'
    redirect_to root_url
  end
end
