import consumer from "./consumer"

$(function() {
  const discussionEndChannel = consumer.subscriptions.create("DiscussionEndChannel", {
    connected() {
      // Called when the subscription is ready for use on the server
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received() {
      $('#timer').remove()
      window.alert('ウルフに投票するんやで')  
    },

    voting(){
      return this.perform('voting')
    },

    wrong() {
      return this.perform('wrong')
    }
  })

  // カウントダウン終了時
  $(document).on('time.elapsed', '#timer', function () {
    if (isWordWolf()) {
      discussionEndChannel.voting()
    } else {
      $('#correct').remove()
      discussionEndChannel.wrong()
    }
  })

  // YesNoウルフは正解後に投票フェーズ
  $(document).on('click', '#correct', function() {
    $('#correct').remove()
    discussionEndChannel.voting()
  })
})

function isWordWolf() {
  return $('#room').data('game-type-name') == 'ワードウルフ'
}
