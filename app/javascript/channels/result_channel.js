import consumer from "./consumer"

$(function() {
  const resultChannel = consumer.subscriptions.create({ channel: "ResultChannel", room_id: $('#room').data('id') }, {
    connected() {
      // Called when the subscription is ready for use on the server
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received(data) {
      $("#result").html(data.result_html)
      $('.vote').prop('disabled', true)
      $('.voted').hide()
      $('#minutes').prop('disabled', false)
      $('#timer').empty()
      $('#theme').empty()
      $('#start').prop('disabled', false)
    },

    vote(user_id) {
      return this.perform('vote', { user_id: user_id })
    }
  })

  $(document).on('click', '.vote', function() {
    let user_id = $(this).data('user-id')
    resultChannel.vote(user_id)
  })
})
