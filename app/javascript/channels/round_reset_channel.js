import consumer from "./consumer"

$(function() {
  const roundResetChannel = consumer.subscriptions.create({ channel: "RoundResetChannel", room_id: $('#room').data('id') }, {
    connected() {
      // Called when the subscription is ready for use on the server
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received() {
      resetRound()
      showResetToast()
    },

    reset() {
      return this.perform('reset')
    }
  })

  $(document).on('click', '#reset', function() {
    if(window.confirm('リセットするかい？')){
      roundResetChannel.reset()
    }
  })
})

// TODO: 場所を再考
function resetRound() {
  $('.vote').prop('disabled', true)
  $('#minutes').prop('disabled', false)
  $('#timer').remove()
  $('#theme').empty()
  $('#start').prop('disabled', false)
  $('#result').empty('')
}

function showResetToast() {
  $.toast({
    text: 'ゲームがリセットされたぞい',
    showHideTransition: 'slide',
    hideAfter: 5000,
    icon: 'info'
  })
}
