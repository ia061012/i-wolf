import consumer from "./consumer"

$(function() {
  const roundStartChannel = consumer.subscriptions.create("RoundStartChannel", {
    connected() {
      // Called when the subscription is ready for use on the server
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received(data) {
      $('#leave').prop('disabled', false)
      $("#theme").html(data.theme_html)
      startTimer(data.minutes)
      $('#result').empty()
      $('#start').prop('disabled', true)
    },

    start(minutes) {
      return this.perform('start', { minutes: minutes })
    }
  })

  $(document).on('click', '#start', function() {
    roundStartChannel.start($('#minutes').val())
  })
})

function startTimer(minutes) {
  $('#result').empty()
  $('#theme').after(`<div id="timer" class="timer-box">0h${minutes}m</div>`)
  $('#timer').countDown({ with_labels: false })
  $('.item-hh').hide()
  $('#timer .separator:first').hide()
}
