import consumer from "./consumer"

$(function() {
  const usersChannel = consumer.subscriptions.create({ channel: "UsersChannel", room_id: $('#room').data('id') }, {
    connected() {
      // Called when the subscription is ready for use on the server
    },

    disconnected() {
      // Called when the subscription has been terminated by the server
    },

    received(data) {
      $("#users").html(data.users_html)
      if(data.hasOwnProperty('kicked')) {
        window.location.href = '/'
      }
      if(data.hasOwnProperty('enter')) {
        showEnterToast(data.enter)
      }
      if(data.hasOwnProperty('leave')) {
        showLeaveToast(data.leave)
      }
    },

    kick(user_id) {
      return this.perform('kick', { user_id: user_id })
    }
  })

  $(document).on('click', '.kick', function() {
    let user_name = $(this).parent().next().text()
    if(!window.confirm(`${user_name}をキックアウトーーーーーー！`)){ return false }
  
    let user_id = $(this).data('user-id')
    usersChannel.kick(user_id)
  })
})

$(document).on('click', '#leave', function(e) {
  if(!window.confirm('退室してよろしいか？')){
    e.preventDefault()
  }
})

function showEnterToast(user_name) {
  $.toast({
    text: `${user_name}が入ってきた！`,
    showHideTransition: 'slide',
    hideAfter: 5000,
    icon: 'info'
  })
}

function showLeaveToast(user_name) {
  $.toast({
    text: `${user_name}が出てった！`,
    showHideTransition: 'slide',
    hideAfter: 5000,
    icon: 'warning'
  })
}
