class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  def errors_html
    errors.full_messages.join('<br>')
  end
end
