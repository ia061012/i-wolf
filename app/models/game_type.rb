class GameType < ApplicationRecord
  has_many :roles

  def word_wolf?
    name == 'ワードウルフ'
  end

  def yes_no_wolf?
    name == 'Yes/Noウルフ'
  end
end
