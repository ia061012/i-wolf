class Role < ApplicationRecord
  belongs_to :game_type

  scope :facilitator, -> { where(name: '司会') }
  scope :wolf, -> { where(name: 'ウルフ') }
  scope :human, -> { where(name: '村人') }

  def human?
    name == '村人'
  end

  def wolf?
    name == 'ウルフ'
  end

  def facilitator?
    name == '司会'
  end
end
