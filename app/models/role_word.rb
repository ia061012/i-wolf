class RoleWord < ApplicationRecord
  belongs_to :role
  belongs_to :word
  belongs_to :round
end
