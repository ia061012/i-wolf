class Room < ApplicationRecord
  belongs_to :host, class_name: 'User'
  belongs_to :game_type
  has_many :users
  has_many :rounds

  validates :name, presence: true, uniqueness: true

  def playing?
    rounds.any?(&:playing?)
  end

  def voting?
    rounds.any?(&:voting?)
  end

  def state
    playing? ? '進行中' : '待機中'
  end

  def current_round
    rounds.order(id: :desc)&.first
  end

  def all_users_has_voted?
    users.any?(&:not_finished_voting?) ? false : true
  end
end
