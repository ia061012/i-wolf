class Round < ApplicationRecord
  STATES = { playing: '進行中', ended: '終了済み', voting: '投票中', aborted: '中止' }.freeze

  belongs_to :room
  has_many :role_words
  has_many :role_words_already_join_roles, -> { where('role_words.role_id = roles.id') }, class_name: :RoleWord
  has_many :user_roles
  has_many :votes
  has_many :votes_already_join_users, -> { where('votes.voter_id = users.id') }, class_name: :Vote

  after_create :setup

  # ラウンドのユーザに役職、ワード、投票先を付加
  scope :result, ->(round) do
    where(id: round).joins(user_roles: [:user, :role])
      .left_joins(role_words_already_join_roles: :word)
      .left_joins(votes_already_join_users: :votee)
      .select('users.name as user_name, roles.name as role_name, words.name as word_name, votees_votes.name vote_to')
  end

  def playing?
    state == STATES[:playing]
  end

  def voting?
    state == STATES[:voting]
  end

  def setup
    rule = room.game_type.word_wolf? ? GameRule::WordWolf.new(self) : GameRule::YesNoWolf.new(self)
    rule.setup!
  end

  def role_and_word user
    role = self.role user
    word = self.word role
    return role, word
  end

  def role user
    user_roles.find_by(user: user)&.role
  end

  def word role
    role_words.find_by(role: role)&.word
  end

  def votee(user)
    votes.find_by(voter: user)&.votee
  end

  def abort
    update_attribute(:state, STATES[:aborted])
  end

  def votees
    votes.map(&:votee)
  end

  def winner
    return if votees.empty? # YesNoの時間切れのため

    votee2vote_size = votees.group_by(&:itself).transform_values(&:size)
    max_vote_size = votee2vote_size.values.max
    most_votees = votee2vote_size.select { |_k, v| v == max_vote_size }.keys
    most_votees.all?(&:wolf?) ? human : wolf
  end

  def wolf
    room.game_type.roles.wolf.first
  end

  def human
    room.game_type.roles.human.first
  end

  def close!
    update_attribute(:state, STATES[:ended])
  end

  def move_to_voting!
    update_attribute(:state, STATES[:voting])
  end
end
