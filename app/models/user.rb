class User < ApplicationRecord
  attr_accessor :remember_token
  has_secure_password

  belongs_to :room, optional: true
  has_many :active_votings, class_name: "Vote", foreign_key: "voter_id"
  has_many :passive_votings, class_name: "Vote", foreign_key: "votee_id"
  has_many :votes, foreign_key: :voter_id
  has_many :attackers, through: :passive_votings, source: :voter
  has_many :targets, through: :active_votings, source: :votee

  validates :name, presence: true, uniqueness: true
  validates :password, presence: true

  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_CONST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  def enter(room)
    update_attribute(:room, room)
  end

  def leave
    update_attribute(:room, nil)
  end

  def host?(room = self.room)
    self == room&.host
  end

  def has_voted?
    votes.find_by(round: room.current_round)
  end

  def not_finished_voting?
    !has_voted?
  end

  def current_role
    room&.current_round&.role(self)
  end

  def wolf?
    current_role.wolf?
  end

  def facilitator?
    current_role&.facilitator?
  end
end
