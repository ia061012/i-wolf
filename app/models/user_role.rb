class UserRole < ApplicationRecord
  belongs_to :round
  belongs_to :user
  belongs_to :role
end
