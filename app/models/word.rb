class Word < ApplicationRecord
  scope :random, -> (count){ order('RANDOM()').limit(count) }
  # 複数の単語を持つカテゴリをランダムに1つ抽出
  scope :category_having_words, -> { group(:category_id).having('COUNT(1) >= 2').select('words.category_id').random(1) }
  # 同じカテゴリからランダムに2つ抽出
  scope :choose2, -> { where(category_id: category_having_words.first.category_id).random(2) }
end
