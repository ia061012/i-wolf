Rails.application.routes.draw do
  root to: 'rooms#index'
  get 'sessions/new'
  delete '/logout', to: 'sessions#destroy'
  post   '/login',  to: 'sessions#create'
  resources :users, only: %i[create]
  resources :rooms
  resources :rounds, only: %i[create]
end
