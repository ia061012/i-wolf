Role.seed(:id,
  { id: 1, name: 'ウルフ', game_type_id: 1 },
  { id: 2, name: '村人', game_type_id: 1 },
  { id: 3, name: 'ウルフ', game_type_id: 2 },
  { id: 4, name: '村人', game_type_id: 2 },
  { id: 5, name: '司会', game_type_id: 2 }
)
