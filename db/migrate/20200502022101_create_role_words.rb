class CreateRoleWords < ActiveRecord::Migration[6.0]
  def change
    create_table :role_words do |t|
      t.integer :role_id
      t.integer :word_id
      t.integer :round_id

      t.timestamps
    end
  end
end
