class CreateRounds < ActiveRecord::Migration[6.0]
  def change
    create_table :rounds do |t|
      t.integer :room_id
      t.string  :state

      t.timestamps
    end
  end
end
