class AddGameTypeIdToRooms < ActiveRecord::Migration[6.0]
  def change
    add_column :rooms, :game_type_id, :integer, after: :host_id
  end
end
