class AddIndexToVotes < ActiveRecord::Migration[6.0]
  def change
    add_index :votes, [:round_id, :voter_id], unique: true
  end
end
