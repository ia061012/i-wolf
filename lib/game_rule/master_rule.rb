class GameRule::MasterRule
  def initialize round
    @round = round
    @num_of_wolf = 1
  end

  def setup!
    create_role_words! # 役職の単語割当
    assign_role!
  end

  def create_role_words!
    raise "create_role_words! method was not defined."
  end

  def assign_role!
    raise "assign_role! method was not defined."
  end
end
