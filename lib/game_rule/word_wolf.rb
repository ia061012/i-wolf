class GameRule::WordWolf < GameRule::MasterRule
  def assign_role!
    wolf = @round.room.game_type.roles.first
    villager = @round.room.game_type.roles.last
    users = @round.room.users.shuffle
    users.map.with_index(1) do |user, idx|
      if idx <= @num_of_wolf
        @round.user_roles.create!(user: user, role: wolf)
      else
        @round.user_roles.create!(user: user, role: villager)
      end
    end
  end

  def create_role_words!
    roles = @round.room.game_type.roles
    words = Word.choose2
    @round.role_words.create!(role: roles[0], word: words[0])
    @round.role_words.create!(role: roles[1], word: words[1])
  end
end
