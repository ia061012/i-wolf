class GameRule::YesNoWolf < GameRule::MasterRule
  def assign_role!
    roles = @round.room.game_type.roles
    users = @round.room.users.shuffle
    users.each.with_index(1) do |user, idx|
      if idx == 1
        @round.user_roles.create!(user: user, role: roles.facilitator.first)
      elsif idx <= 1 + @num_of_wolf
        @round.user_roles.create!(user: user, role: roles.wolf.first)
      else
        @round.user_roles.create!(user: user, role: roles.human.first)
      end
    end
  end

  def create_role_words!
    word = Word.random(1).first
    @round.room.game_type.roles.each do |role|
      @round.role_words.create!(role: role, word: word) unless role.human?
    end
  end
end
